﻿First, clone repository.

git clone https://vinu_havock@bitbucket.org/vinu_havock/zoonman-linkedin-api-php-client.git
Change dir to the repo

cd zoonman-linkedin-api-php-client
Install dependencies:

composer install [-d /path/to/repository/root]
If you don't have composer, you can get it here. Parameters in brackets are optional.

Create .env file with linkedin credentials in the parent catalog (in the repository root) like this

LINKEDIN_CLIENT_ID=111ClientId111
LINKEDIN_CLIENT_SECRET=222ClientSecret
The simplest way to do that to run the following commands:

echo 'LINKEDIN_CLIENT_ID=111ClientId111' >> .env
echo 'LINKEDIN_CLIENT_SECRET=222ClientSecret' >> .env
To get client and secret go to LinkedIn Developers portal and create new app there.

After add to OAuth 2.0 Authorized Redirect URLs:

http://localhost:8901/
Next, run PHP embedded server in the repository root:

php -S localhost:8901 -t examples
Navigate to http://localhost:8901/